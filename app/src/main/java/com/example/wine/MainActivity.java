package com.example.wine;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.content.Intent;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView.Adapter;

public class MainActivity extends AppCompatActivity {

    ListView myListView;
    Cursor result;
    WineDbHelper dbHelper;
    SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            /*@Override
            public void onClick(View view) {
                Snackbar.make(view, "Add new Wine", Snackbar.LENGTH_LONG)
                        .setAction("Add new Wine", new View.OnClickListener() {*/
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(getApplicationContext(), WineActivity.class);
                                intent.putExtra("nouveau", "newWine");
                                startActivityForResult(intent,1);

                            }
                        //}).show();
            //}
        });

        dbHelper = new WineDbHelper(this);
        Log.d("AFF", "nb row : " + dbHelper.fetchAllWines().getString(0));
        if(dbHelper.fetchAllWines().getCount() < 1)
        {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            dbHelper.populate(db);
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //Cursor result = db.query(WineDbHelper.TABLE_NAME, new String[]{WineDbHelper._ID, WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, null, null, null, null, WineDbHelper.COLUMN_NAME);
        result = dbHelper.fetchAllWines();
        /*final SimpleCursorAdapter*/ adapter = new SimpleCursorAdapter(this, R.layout.double_ligne_activity, result, new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, new int[]{R.id.name, R.id.region}, 0);

        myListView = (ListView) findViewById(R.id.list);
        myListView.setAdapter(adapter);

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View V, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), WineActivity.class);
                result.moveToPosition(position);

                //Parcelable parse = WineDbHelper.cursorToWine(result);

                Wine vin = WineDbHelper.cursorToWine(result);
                //Cursor vin = adapter.getItem(position);
                //Wine vin2 = WineDbHelper.cursorToWine(vin);
                //Object vin = parent.getItemAtPosition(position);
                //String message =;
                //Cursor vin = (Cursor) ((ListView) parent).getItemAtPosition(position);
                //Log.d("INTENT", "value : "+vin.getString(0));
                //Wine vin2 = WineDbHelper.cursorToWine(vin);
                intent.putExtra("WineName", vin);
                startActivityForResult(intent, 1);
            }

        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data != null){
            Wine vin = data.getParcelableExtra("wine");
            //dbHelper.fetchAllWines();
            adapter.changeCursor(dbHelper.getReadableDatabase().query(WineDbHelper.TABLE_NAME, null,null, null, null, null, null));
            adapter.notifyDataSetChanged();
        }

    }

   @Override
    public void onResume(){
        super.onResume();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
