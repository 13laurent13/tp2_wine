package com.example.wine;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

	// db.execSQL() with the CREATE TABLE ... command
        String req = "create table "+TABLE_NAME+" ( "+_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+COLUMN_NAME+" text NOT NULL, "+COLUMN_WINE_REGION+" text, "+COLUMN_LOC+" text, "+COLUMN_CLIMATE+" text, "+COLUMN_PLANTED_AREA+" text, UNIQUE("+COLUMN_NAME+", "+COLUMN_WINE_REGION+") ON CONFLICT ROLLBACK)";
        db.execSQL(req);
        populate(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME); // drops the old database
        onCreate(db);

    }


   /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine, SQLiteDatabase db) {
        //SQLiteDatabase db = this.getWritableDatabase();

        Cursor record = db.rawQuery("select * from ? where name = ? and region = ?", new String[]{TABLE_NAME, wine.getTitle(), wine.getRegion()});

        if(record.getCount() > 0){
            record.close();
            db.close();
            return false;
        }
        else{
            ContentValues values = new ContentValues();

            values.put(COLUMN_NAME, wine.getTitle());
            values.put(COLUMN_WINE_REGION, wine.getRegion());
            values.put(COLUMN_LOC, wine.getLocalization());
            values.put(COLUMN_CLIMATE, wine.getClimate());
            values.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());

            db.insert(TABLE_NAME, null, values);
            //db.close();
            return true;
        }

        // Inserting Row
        //long rowID = 0;
	// call db.insert()
        //rowID = db.insert(TABLE_NAME, null, values);
        //db.close(); // Closing database connection

        //return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
	int res = 0;

        // updating row
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, wine.getTitle());
        contentValues.put(COLUMN_WINE_REGION, wine.getRegion());
        contentValues.put(COLUMN_LOC, wine.getLocalization());
        contentValues.put(COLUMN_CLIMATE, wine.getClimate());
        contentValues.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
	// call db.update()
        db.update(TABLE_NAME, contentValues, "_id == ?", new String[]{ Long.toString(wine.getId())});
        db.close();
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getWritableDatabase();

        // call db.query()
        Cursor cursor = db.query(TABLE_NAME, null, null, null,null,null,null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

     public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
         Wine vin = cursorToWine(cursor);
         db.delete(TABLE_NAME, "_id == ?", new String[]{ Long.toString(vin.getId())});
        db.close();
    }

     public void populate(SQLiteDatabase db) {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"),db);
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"),db);
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"),db);
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"),db);
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"),db);
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"),db);
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"),db);
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"),db);
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"),db);
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"),db);
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"),db);


        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        //db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        Wine wine = null;
	// build a Wine object from cursor
        long id = cursor.getInt(cursor.getColumnIndex("_id"));
        String nom = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
        String region = cursor.getString(cursor.getColumnIndex(COLUMN_WINE_REGION));
        String loc = cursor.getString(cursor.getColumnIndex(COLUMN_LOC));
        String climate = cursor.getString(cursor.getColumnIndex(COLUMN_CLIMATE));
        String publ = cursor.getString(cursor.getColumnIndex(COLUMN_PLANTED_AREA));

        wine = new Wine(id, nom, region, loc, climate, publ);

        return wine;
    }
}
