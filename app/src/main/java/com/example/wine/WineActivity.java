package com.example.wine;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.app.AlertDialog;

public class WineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        Intent intent = getIntent();

        final WineDbHelper dbHelper = new WineDbHelper(this);
        final SQLiteDatabase db = dbHelper.getReadableDatabase();

        if (intent.hasExtra("WineName")) {


            //String wineName = "";

            //wineName = intent.getStringExtra("MESSAGE");

            final Wine vin = intent.getParcelableExtra("WineName");

            EditText nom = (EditText) findViewById(R.id.wineName);
            nom.setText(vin.getTitle());

            //final int ident = cursor.getInt(0);

            EditText winereg = (EditText) findViewById(R.id.editWineRegion);
            winereg.setText(vin.getRegion());

            EditText loc = (EditText) findViewById(R.id.editLoc);
            loc.setText(vin.getLocalization());

            EditText climat = (EditText) findViewById(R.id.editClimate);
            climat.setText(vin.getClimate());

            EditText plant = (EditText) findViewById(R.id.editPlantedArea);
            plant.setText(vin.getPlantedArea());

            Button button = findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View V) {
                    WineDbHelper dbH = new WineDbHelper(getApplicationContext());

                    Wine vin_up = new Wine(vin.getId(), ((EditText) findViewById(R.id.wineName)).getText().toString(), ((EditText) findViewById(R.id.editWineRegion)).getText().toString(), ((EditText) findViewById(R.id.editLoc)).getText().toString(), ((EditText) findViewById(R.id.editClimate)).getText().toString(), ((EditText) findViewById(R.id.editPlantedArea)).getText().toString());
                    dbH.updateWine(vin_up);
                    if(((EditText) findViewById(R.id.wineName)).getText().toString().isEmpty()){
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WineActivity.this);
                        alertDialogBuilder.setTitle("Sauvegarde impossible");
                        alertDialogBuilder.setMessage("Nom du vin vide");
                        alertDialogBuilder.create();
                        alertDialogBuilder.show();

                    }
                    else{
                        Intent intent = new Intent(WineActivity.this, MainActivity.class);
                        intent.putExtra("wine", vin);
                        /*if(add){
                            intent.putExtra("add", true);
                        }else{
                            intent.putExtra("add", false);
                        }*/
                        setResult(1,intent);
                        finish();

                    }
                    //Intent resint = new Intent();
                    //resint.putExtra("update", 1);
                    //setResult(2,resint);
                    finish();

                }
            });

        } else if (intent.hasExtra("nouveau")) {
            EditText nom = (EditText) findViewById(R.id.wineName);
            nom.setText("Appellation");

            Button button = findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View V) {
                    WineDbHelper dbH = new WineDbHelper(getApplicationContext());

                    Wine vin = new Wine(findViewById(R.id.wineName).toString(), findViewById(R.id.editWineRegion).toString(), findViewById(R.id.editLoc).toString(), findViewById(R.id.editClimate).toString(), findViewById(R.id.editPlantedArea).toString());
                    dbH.addWine(vin,db);

                }
            });
        }
    }
}